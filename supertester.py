import sys
from scapy.all import *
import datetime
import wget
import os
import random
import timeout_decorator

previous_date = None
total = 0.00
success = 0.00
downloaded_total = 0.00
last_ping = None

files = [
    'https://speed.hetzner.de/100MB.bin',
    'http://ipv4.download.thinkbroadband.com/5MB.zip',
    'http://mirror.filearena.net/pub/speed/SpeedTest_16MB.dat',
    'http://mirror.filearena.net/pub/speed/SpeedTest_32MB.dat',
    'http://ipv4.download.thinkbroadband.com/5MB.zip',
    'http://ipv4.download.thinkbroadband.com/10MB.zip',
    'http://ipv4.download.thinkbroadband.com/20MB.zip',
    'http://speedcheck.cdn.on.net/10meg.test',
    'http://speedcheck.cdn.on.net/50meg.test',
    'http://212.183.159.230/10MB.zip',
    'http://212.183.159.230/20MB.zip',
    'http://212.183.159.230/50MB.zip',
]

def check_ping():
    hostname = "google.com"
    response = os.system("ping -c 1 -W 1 " + hostname)
    if response == 0:
        return True
    else:
        return False

@timeout_decorator.timeout(120)
def fetch_file(downloaded_total):
    print("Fetching file...")
    website = files[random.randint(0,len(files)-1)]
    filename = wget.download(website, out='tmp/tempfile')
    size = b = os.path.getsize(filename)
    downloaded_total += (size/1000.00)
    os.remove(filename)
    return downloaded_total

while True:
    today = datetime.datetime.today()
    date = today.day
    month = today.month
    if date != previous_date:
        previous_date = date
        total = 0.00
        success = 0.00
        downloaded_total = 0.00

    result = check_ping()

    if not result:
        print("This host is down")
    else:
        print("This host is up")
        success += 1.00
        last_ping = today
        try:
            downloaded_total = fetch_file(downloaded_total)
        except Exception as e:
            print('Timed out fetching file')
            print(e)
            continue

    os.system('rm -rf tmp/*.tmp')
    total += 1.00
    percent = (success/total) * 100
    print("Internet on {}-{}: {}%".format(month, date, percent))
    print("Successful pings: {}".format(success))
    print("Total downloaded: {}kB".format(downloaded_total))
    print("Last Connection: {}".format(last_ping))
    time.sleep(1)
